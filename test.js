/**
 * Thingshub Javascript Driver Test Script
 *   - testPayloads: an array of strings, raw payloads from sensor to test
 *   - port: an integer value in range [0, 255], f_port value from sensor
 *
 * Install prerequisites:
 * npm install var_dump --save-dev
 *
 * Scroll down to define your own payloads.
 *
 */

var testPayloads = [
    '10000000003c1b3a003c1b3a003c1a3a003c1a3a',
  ];

var port = 1;

const var_dump = require('var_dump');
var fs = require('fs');
console.log("[ ] Loading the driver...");
eval(fs.readFileSync('script.js')+'');

function hexToBytes(hex) {
    for (var bytes = [], c = 0; c < hex.length; c += 2)
        bytes.push(parseInt(hex.substr(c, 2), 16));
    return bytes;
}

testPayloads.forEach(function(payload) {
    console.log("\n[ ] Testing with Payload '" + payload + "'");
    var_dump(decode(hexToBytes(payload),port));
});

console.log("[x] Done.");
