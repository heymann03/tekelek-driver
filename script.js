/**
 * Decode function is the main entry point in the script,
 * and you should not delete it.
 *
 * Arguments:
 * 'payload' - an array of bytes, raw payload from sensor
 * 'port' - an integer value in range [0, 255], f_port value from sensor
 *
 * TODO:
 * explain example payload
 */

// driver for measuring comple sample payload
var m1 = new Object();
var m2 = new Object();
var m3 = new Object();
var m4 = new Object();

function measurement(chunkedPayload) {
    var measuredValues = new Object();
    // calculates ullage in cms and temperature in celcius
    var level1 = chunkedPayload[0]
    var level2 = chunkedPayload[1]
    var temp = chunkedPayload[2]
    measuredValues.Ullage = (level2 + (level1 * 256));
    measuredValues.Temperature = temp < 32 ? temp : temp - 256;
    // decodes SRC and SRSSI
    // SRC:     explain
    // SRSSI:   explain
    var srcsrssi = parseInt(chunkedPayload[3], 10).toString(2);
    var src = parseInt(srcsrssi.slice(0, 3), 2).toString(10);
    measuredValues.SRC = src;
    var srssi = parseInt(srcsrssi.slice(3), 2).toString(10);
    measuredValues.SRSSI = srssi;
    return {
        "measuredValues": measuredValues
    }
}

function decode(payload, port) {
    var result = new Object();
    if (payload[1] != 00) {
        console.log("\nNot TEK 766 payload\n");
        // TODO: escalate here - https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Statements/throw
    } else {
        console.log("\nTEK 766 payload\n")
    }

    // TODO: replace if's with switch case - https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Statements/switch
    // MEASUREMENT PAYLOAD
    if (payload[0] == 16) {
        console.log("valid payload, Measurement");
        result.payloadType = 'Measurement';

        // payload describes alarm
        if (payload[2] != 0) {
            console.log("Alarm Limit Reached");
            var lim = (parseInt(payload[2], 16).toString(2))
            // TODO: explain slice
            lim = lim.slice(4);
            var x = (lim & 1) ? "Alarm Reached Level-1" : "Alarm Not-ReachedReached Level-1"
            console.log(x);
            //result.Alarm = x;
            var y = (lim & 2 & 3) ? "Alarm Reached Level-2" : "Alarm Not-Reached Level-2";
            console.log(y);
            //result.Alarm = y;
            var z = (lim & 4 & 5 & 6 & 7) ? "Alarm Reached Level-3" : "Not Reached Level-3";
            console.log(z);
            result.Alarm = x + ',' + y + ',' + z;
        } else {
            result.Alarm = 'Alarm Level Not-Reached';
        }

        // decoding four measurements
        m1 = measurement(payload.slice(4, 8));
        m2 = measurement(payload.slice(8, 12));
        m3 = measurement(payload.slice(12, 16));
        m4 = measurement(payload.slice(16, 20));


        return {
            "m1": m1,
            "m2": m2,
            "m3": m3,
            "m4": m4,
            "result": result,
            "payload": payload,
            "port": port
        }
    }

    // STATUS PAYLOAD
    if (payload[0] == 48) {
        console.log("valid payload, Status\n");
        result.payloadType = 'Status';

        // defines hardwareID and firmwareRevision
        var hwid = (parseInt(payload[3], 16).toString(10));
        result.hardwareID = hwid;
        var fMRev = payload[4]; //fMRev = firmwareMainRevision
        var fSRev = payload[5]; //fSRev = firmwareSubRevision
        result.firmwareRevision = fMRev + '.' + fSRev;

        // converting the payload into binary and detecting the reason
        // TODO: explain what is a reason and for what reason. reason of status?
        var reason = parseInt(payload[6], 16).toString(2);
        if (reason != 0) {
            var mContact = reason.substring(4);
            var manualContact = (mContact & 2) ? 'Manual Contact' : 'No Manual Contact';
            var sRequest = reason.substring(1, 4);
            var systemRequest = (sRequest & 5) ? 'System Request Reset' : 'Not a System Request';
            var active = reason.substring(0, 1);
            var Active = (active & 1) ? 'Active' : 'Not Active';
            result.Reason = manualContact + ',' + systemRequest + ',' + Active;
        } else {
            console.log(" No Reset Occured");
        }
        // defines the signal strength and remaining battery in %
        var rssi = payload[8];
        result.RSSI = ('-' + rssi);
        var battery = payload[10];
        result.Battery = battery + '%';
        // defines measurementSections in minutes and scheduled transmit period in hours
        var measurement1 = (parseInt((payload[11]), 16).toString(10) * 256);
        var measurement2 = parseInt((payload[12]), 16).toString(10);
        result.mSections = measurement1 + measurement2 + ' min';
        var txperiod = payload[13];
        result.txPeriod = (txperiod + ' hours');

        // defines measurement made by the sensor in status payload
        m1 = measurement(payload.slice(14));
        return {
            "m1": m1,
            "result": result,
            "payload": payload,
            "port": port
        }
    }

    // ALARM PAYLOAD
    if (payload[0] == 69) {
        console.log("valid payload, Alarm");
        result.payloadType = 'Alarm';

        // payload describes alarm and the limit exceeded
        if (payload[2] != 0) {
            console.log("Alarm Limit Reached");
            var lim = (parseInt(payload[2], 16).toString(2))
            // TODO: show slice with example
            lim = lim.slice(4);
            // TODO: if(lim == 1) // if(lim <= 3) // if(lim >= 4) OR EXPLAIN WHY THIS WAY WITH x,y,z.
            var x = (lim & 1) ? "Alarm Reached Level-1" : "Alarm Not-Reached Level-1"
            var y = (lim & 2 & 3) ? "Alarm Reached Level-2" : "Alarm Not-Reached Level-2";
            var z = (lim & 4 & 5 & 6 & 7) ? "Alarm Reached Level-3" : "Alarm Not Reached Level-3";
            result.Alarm = x + ',' + y + ',' + z;
        } else {
            result.Alarm = 'Alarm Level Not-Reached';
        }

        // measurement 1 starts here
        // TODO: explain what you slice here
        m1 = measurement(payload.slice(4, 8));
        m2 = measurement(payload.slice(8));

        return {
            "m1": m1,
            "m2": m2,
            "result": result,
            "payload": payload,
            "port": port
        }
    }
}
